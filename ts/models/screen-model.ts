// Using color names from https://en.wikipedia.org/wiki/ANSI_escape_code#3-bit_and_4-bit
export enum colors {
    black,
    red,
    green,
    yellow,
    blue,
    magenta,
    cyan,
    white,
    bright_black,
    bright_red,
    bright_green,
    bright_yellow,
    bright_blue,
    bright_magenta,
    bright_cyan,
    bright_white
}

export interface RowColumn {
    bg?: colors | null;     // Background Color
    fc?: colors | null;     // Foreground Color
    blink?: boolean | null; // Blink?
    char?: string | null;   // Character
}

export interface Row {
    columns: Array<RowColumn>;
}

export interface Layer {
    rows: Array<Row>;
}

export interface Layers {
    layers: Array<Layer>;
}
