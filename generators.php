<?php

$colors = [
    'black',
    'red',
    'green',
    'yellow',
    'blue',
    'magenta',
    'cyan',
    'white',
    'bBlack',
    'bRed',
    'bGreen',
    'bYellow',
    'bBlue',
    'bMagenta',
    'bCyan',
    'bWhite',
];

// Row and Columns test.
for ($r = 0; $r < 24; ++$r) {
    $rColor = $r < 16 ? $r : $r-16;

    echo "            <div id=\"r{$r}\" class=\"row\">\n";

    for ($c = 0; $c < 80; ++$c) {
        if ($c < 16) {
            echo "                <div id=\"r{$r}c{$c}\" class=\"col c{$c} bgc-{$colors[$rColor]} fgc-{$colors[$c]}\">*</div>\n";
        } elseif ($c < 32) {
            echo "                <div id=\"r{$r}c{$c}\" class=\"col c{$c} bgc-{$colors[$rColor]} fgc-{$colors[$c-16]}-blink\">*</div>\n";
        } else {
            echo "                <div id=\"r{$r}c{$c}\" class=\"col c{$c} bgc-{$colors[$rColor]}\"> </div>\n";
        }
    }

    echo "        </div>\n";
}


